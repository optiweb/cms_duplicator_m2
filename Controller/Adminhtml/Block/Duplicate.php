<?php

namespace Optiweb\Duplicator\Controller\Adminhtml\Block;

use Magento\Cms\Model\Block;
use Magento\Framework\Exception\LocalizedException;

class Duplicate extends \Magento\Cms\Controller\Adminhtml\Block
{
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $blockId = $this->getRequest()->getParam('block_id');
        if ($blockId) {
            /** @var \Magento\Cms\Model\Block $oldBlock */
            $oldBlock = $this->_objectManager->create(Block::class)->load($blockId);

            if (!$oldBlock->getId()) {
                $this->messageManager->addErrorMessage(__('This block no longer exists.'));
                return $resultRedirect->setPath('cms/block/');
            }

            $newBlock = $this->_objectManager->create(Block::class);
            $oldBlock->setBlockId(null) // remove old block id so a new one is assigned
                ->setTitle($oldBlock->getTitle() . ' - Copy')
                ->setIdentifier($oldBlock->getIdentifier() . '_copy')
                ->setCreationTime(null)
                ->setUpdateTime(null)
                ->setIsActive(0);
            $newBlock->setData($oldBlock->getData());

            try {
                $newBlock = $newBlock->save();
                $this->messageManager->addSuccessMessage(__('You duplicated the block.'));
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Throwable $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while duplicating the block.'));
            }
            return $resultRedirect->setPath('cms/block/edit', ['block_id' => $newBlock->getId()]);
        }
        $this->messageManager->addWarningMessage(__('Block id not provided.'));
        return $resultRedirect->setPath('cms/block/index');
    }
}
