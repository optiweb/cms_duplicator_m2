<?php

namespace Optiweb\Duplicator\Controller\Adminhtml\Page;

use Magento\Cms\Model\Page;
use Magento\Framework\Exception\LocalizedException;

class Duplicate extends \Magento\Backend\App\Action
{
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $pageId = $this->getRequest()->getParam('page_id');
        if ($pageId) {
            /** @var \Magento\Cms\Model\Page $oldPage */
            $oldPage = $this->_objectManager->create(Page::class)->load($pageId);
            if (!$oldPage->getId()) {
                $this->messageManager->addErrorMessage(__('This page no longer exists.'));
                return $resultRedirect->setPath('cms/page/');
            }

            $newPage = $this->_objectManager->create(Page::class);

            $oldPage->setIdentifier($oldPage->getIdentifier() . '-copy')
                ->setPageId(null)
                ->setTitle($oldPage->getTitle() . ' - Copy')
                ->setCreationTime(null)
                ->setUpdateTime(null)
                ->setIsActive(0);

            $newPage->setData($oldPage->getData());

            try {
                $newPage = $newPage->save();
                $this->messageManager->addSuccessMessage(__('You duplicated the page.'));
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Throwable $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while duplicating the page.'));
            }
            return $resultRedirect->setPath('cms/page/edit', ['page_id' => $newPage->getId()]);
        }
        $this->messageManager->addWarningMessage(__('Page id not provided.'));
        return $resultRedirect->setPath('cms/page/index');
    }
}