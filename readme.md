#Magento 2 - CMS duplicator
This simple extensions allows you to quicky duplicate CMS content. It will duplicate all data including, but not limited to:
- CMS block/page store views
- title and content
- identitfier (read known issues below)
- ...
- status is set to `disabled` by default. You must enable the new entry manually.

---
##Features:
- Duplicate CMS blocks
- Duplicate CMS pages

##Compatibility
Developed on 2.1.8, tested on 2.2.3 and 2.2.4. In all cases Luma theme was used with very few additional extensions. Different themes shouldn't affect compatibility but your mileage may vary.

##Known issues
- duplicated entry's identifier gets `_copy` appended to it. Therefore if you try to duplicate the original entry again, Magento will try to create another block with same id and throw an error.

##Installation:
0. install module: `composer require optiweb/cms_duplicator_m2`
10. enable module: `php bin/magento module:enable Optiweb_Duplicator`
20. run `php bin/magento setup:upgrade` from the root directory of your Magento installation

##Usage
This could hardly be easier ☺
0.  Find a block/page you want to duplicate
10. In the right dropdown menu click `Duplicate` (duh...)
20. Wait a couple of seconds
30. Profit

##Changelog
- `1.0.0` initial release
- `1.0.2` remove extra `__construct()` methods
- `1.0.4` duplicating the block now redirect you to the block/page editing screen
- `1.0.5` prevent some (unlikely) unhandled exceptions where a variable might not have been set, replace some deprecated methods (`addSuccess()`...), add PHP 7.1 support 
- `1.0.6` add PHP 7.2 support, remove PHP 5.X support

##Licence:
MIT. (see `LICENCE.txt`) 