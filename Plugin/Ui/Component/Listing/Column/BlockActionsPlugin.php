<?php

namespace Optiweb\Duplicator\Plugin\Ui\Component\Listing\Column;

use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Cms\Ui\Component\Listing\Column\BlockActions;

class BlockActionsPlugin extends BlockActions
{
    const ULR_PATH_DUPLICATE = 'duplicator/block/duplicate';

    public function afterPrepareDataSource(BlockActions $subject, array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                if (isset($item['block_id'])) {
                    if (!isset($item['actions'])) {
                        $item['actions'] = [];
                    }
                    $item['actions']['duplicate'] = [
                        'href' => $this->urlBuilder->getUrl(
                            static::ULR_PATH_DUPLICATE,
                            [
                                'block_id' => $item['block_id']
                            ]
                        ),
                        'label' => __('Duplicate')
                    ];
                }
            }

        }
        return $dataSource;
    }
}